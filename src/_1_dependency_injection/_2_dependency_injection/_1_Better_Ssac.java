package _1_dependency_injection._2_dependency_injection;

interface Teacher {
  void doLecture();
}

class TeacherSpring implements Teacher {
  @Override
  public void doLecture() {
    System.out.println("Teach spring");
  }
}

class Ssac {
  Teacher teacher;

  public Ssac(Teacher teacher) {
    this.teacher = teacher;
  }

  public void doClass() {
    teacher.doLecture();
  }
}

public class _1_Better_Ssac {
  public static void main(String[] args) {
    Ssac ssac = new Ssac(new TeacherSpring());
    ssac.doClass();
  }
}
