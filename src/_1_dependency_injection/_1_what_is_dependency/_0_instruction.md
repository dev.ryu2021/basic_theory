# What is Dependency?

현재 TeacherJSP가 Ssac에서 수업을 진행하고 있습니다. <br />
학생들은 Spring을 배우고 싶으나 JSP위주로 수업이 진행되고 있어서, 불만이 많은 상태입니다. 

**TeacherSpring을 자유롭게 구현해서 학생들이 Spring을 공부할 수 있게 만들어주세요.**


### Todo
> _2_TestDriver.java 클래스에서 작업을 진행해주세요. <br />


[ ] _2_TestDriver 클래스의 main 함수 속의 주석을 제거 하고 작동하도록 해주세요. <br />
[ ] TeacherSpring 클래스를 작성해주세요. (메소드는 자유, Spring을 가르쳐준다는 의미를 전달할 수 있으면 됩니다.) <br /> 
 